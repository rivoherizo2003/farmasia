<?php


namespace App\DataTransformer\Drug;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Drug\CreateDrugInput;
use App\Entity\Drug;
use DateTime;
use Symfony\Component\VarDumper\Cloner\Data;

/**
 * Class CreateDrugDataTransformer
 * @package App\DataTransformer\Drug
 */
final class CreateDrugDataTransformer implements DataTransformerInterface
{
    /**
     * @param string $to
     * @param array $context
     * @return Drug
     */
    public function transform($object, string $to, array $context = []): Drug
    {
        $drug = new Drug();
        $drug->setCode($object->code);
        $drug->setBnfCode($object->bnfCode);
        $drug->setName($object->name);
        $drug->setItems($object->items);
        $drug->setNic($object->nic);
        $drug->setPrice($object->price);
        $drug->setQuantity($object->quantity);
        $drug->setExpirationDate($object->expirationDate);
        $drug->setNotice(null);

        return $drug;
    }

    /**
     * @param array|object $data
     * @param string $to
     * @param array $context
     * @return bool
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Drug) {
            return false;
        }

        return Drug::class === $to && ($context['input']['class'] ?? null) === CreateDrugInput::class;
    }
}
