<?php


namespace App\Dto\Drug;


final class CreateDrugInput
{
    public ?string $code;

    public ?string $bnfCode;

    public ?string $name;

    public ?int $items;

    public ?float $nic;

    public ?float $price;

    public ?float $quantity;

    public ?\DateTimeInterface $expirationDate;

    public ?string $notice;

    public ?string $pictureBase64;

    public ?string $pictureOriginalName;
}
