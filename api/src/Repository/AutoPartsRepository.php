<?php

namespace App\Repository;

use App\Entity\AutoParts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AutoParts|null find($id, $lockMode = null, $lockVersion = null)
 * @method AutoParts|null findOneBy(array $criteria, array $orderBy = null)
 * @method AutoParts[]    findAll()
 * @method AutoParts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AutoPartsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AutoParts::class);
    }

    // /**
    //  * @return AutoParts[] Returns an array of AutoParts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AutoParts
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
