<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Drug\CreateDrugInput;
use App\Dto\Drug\DrugOutput;
use App\Repository\DrugRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DrugRepository::class)]
#[ApiResource(
    collectionOperations: [
        "create" => [
            "method" => "POST",
            "input" => CreateDrugInput::class,
            "output" => DrugOutput::class
        ],
        "read" => [
            "method" => "GET",
            "output" => DrugOutput::class
        ]
    ],
    attributes: ["pagination_client_enabled" => true, "pagination_client_items_per_page" => true,
    "pagination_items_per_page" => 20]
)]
#[ApiFilter(SearchFilter::class, properties: ['name' => 'iword_start'])]
class Drug extends Product
{

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $notice;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $expirationDate;

    public function getNotice(): ?string
    {
        return $this->notice;
    }

    public function setNotice(?string $notice): self
    {
        $this->notice = $notice;

        return $this;
    }

    public function getExpirationDate(): ?\DateTimeInterface
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(?\DateTimeInterface $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }
}
