<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
#[ORM\HasLifecycleCallbacks()]
#[ORM\InheritanceType("SINGLE_TABLE")]
#[ORM\DiscriminatorColumn(name: "discr", type: "string")]
#[ORM\DiscriminatorMap(["drug" => "Drug", "auto_parts" => "AutoParts"])]
abstract class Product
{
    use StoryEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "IDENTITY")]
    #[ORM\Column(type: 'integer', unique: true)]
    private $id;

    #[ORM\Column(type: 'string', length: 10)]
    private ?string $code;

    #[ORM\Column(type: 'string', length: 30, nullable: true)]
    private ?string $bnfCode;

    #[ORM\Column(type: 'string', length: 200, nullable: true)]
    private ?string $name;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $items;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $nic;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $price;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getBnfCode(): ?string
    {
        return $this->bnfCode;
    }

    public function setBnfCode(?string $bnfCode): self
    {
        $this->bnfCode = $bnfCode;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getItems(): ?int
    {
        return $this->items;
    }

    public function setItems(?int $items): self
    {
        $this->items = $items;

        return $this;
    }

    public function getNic(): ?float
    {
        return $this->nic;
    }

    public function setNic(?float $nic): self
    {
        $this->nic = $nic;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(?float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
