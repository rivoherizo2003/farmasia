<?php


namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait StoryEntity
 * @package App\Entity
 */
trait StoryEntity
{

    #[ORM\Column(type: "datetime", nullable: true)]
    private DateTimeInterface $addedDate;

    #[ORM\Column(type: "datetime", nullable: true)]
    private DateTimeInterface $lastUpdatedDate;

    /**
     * @return DateTimeInterface
     */
    public function getAddedDate(): \DateTimeInterface
    {
        return $this->addedDate;
    }

    /**
     * @param DateTimeInterface $addedDate
     */
    public function setAddedDate(DateTimeInterface $addedDate): void
    {
        $this->addedDate = $addedDate;
    }

    #[ORM\PrePersist]
    public function persistCalled()
    {
        $this->addedDate = new \DateTime();
    }

    /**
     * @return DateTimeInterface
     */
    public function getLastUpdatedDate(): DateTimeInterface
    {
        return $this->lastUpdatedDate;
    }

    /**
     * @param DateTimeInterface $lastUpdatedDate
     */
    public function setLastUpdatedDate(DateTimeInterface $lastUpdatedDate): void
    {
        $this->lastUpdatedDate = $lastUpdatedDate;
    }

    #[ORM\PreUpdate]
    public function updateCalled()
    {
        $this->lastUpdatedDate = new \DateTime();
    }
}
