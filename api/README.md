# API

The API will be here.

Refer to the [Getting Started Guide](https://api-platform.com/docs/distribution) for more information.

copy data set to container
============
```bash
docker cp $path_csv/201701scripts_sample.csv 0f30ef38f4fc:/var/lib/postgresql/data
```

Load csv to database postgresql
==================

```sql
COPY drug(code, bnf_code, name, items, nic, price, quantity, discr, notice, expiration_date)
    FROM '/var/lib/postgresql/data/201606scripts_sample.csv'
    DELIMITER ','
    CSV HEADER;
```

Data sets from kaggle
==========
https://www.kaggle.com/xtianvillaruz/british-nhs-prescription-drugs
