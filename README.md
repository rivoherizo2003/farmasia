<h1 align="center"><a href="https://api-platform.com"><img src="https://api-platform.com/logo-250x250.png" alt="API Platform"></a></h1>

API Platform is a next-generation web framework designed to easily create API-first projects without compromising extensibility
and flexibility:

Pull images and start containers:
==================
```bash
docker-compose build --pull --no-cache && docker-compose up -d
```

API documentation
================
http://localhost:8012/docs
